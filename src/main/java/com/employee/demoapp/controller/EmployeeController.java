package com.employee.demoapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.model.EmployeeModel;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api")
public class EmployeeController {

	private List<EmployeeModel> employeeList;

	public EmployeeController() {
		employeeList = new ArrayList<>();
		fillEmployeeList(employeeList);
	}

	private void fillEmployeeList(List<EmployeeModel> employeeList2) {
		for (int i = 0; i < 10; i++) {
			EmployeeModel employee = new EmployeeModel();
			employee.setFirstName("firstName" + i);
			employee.setLastName("lastName" + i);
			employee.setMiddleName("middleName" + i);
			employee.setAddress("address" + i);
			employee.setMobileNumber("mobileNumber" + i);
			employee.setEmail("email" + i);
			employeeList2.add(employee);
		}
	}

	@GetMapping("/employee/all")
	public List<EmployeeModel> getEmployeeDetails() {
		return employeeList;
	}

	@PostMapping(path = "/employee/join", consumes = MediaType.APPLICATION_JSON_VALUE)
	public boolean addEmployeeDetails(@RequestBody EmployeeModel employee) {
		this.employeeList.add(employee);
		return true;
	}

	@DeleteMapping("/employee/resign/{employeeName}")
	public boolean removeEmployeeDetails(@PathVariable("employeeName") String employeeName) {
		for (EmployeeModel employeeModel : employeeList) {
			if (employeeModel.getFirstName().equals(employeeName)) {
				employeeList.remove(employeeModel);
				return true;
			}
		}
		return false;
	}

}
